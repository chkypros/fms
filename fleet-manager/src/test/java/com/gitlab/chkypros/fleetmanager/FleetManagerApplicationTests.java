package com.gitlab.chkypros.fleetmanager;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = {
                "listeners=PLAINTEXT://localhost:19092",
                "port=19092"
        }
)
class FleetManagerApplicationTests {

    @Test
    void contextLoads() {
    }

}
