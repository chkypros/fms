insert into driver(id, national_id, first_name, last_name, speeding_tendency) values (1, '12A23', 'Kypros', 'Chrysanthou', 50);
insert into driver(id, national_id, first_name, last_name, speeding_tendency) values (2, '45B56', 'Kwstas', 'Kwsta', 30);

insert into car(id, brand, model, licence_plate, automatic, next_service_date) values (1, 'Toyota', 'Axio', 'ABC123', true, DATEADD(month, 1, CURRENT_DATE));
insert into car(id, brand, model, licence_plate, automatic, next_service_date) values (2, 'BMW', '330i', 'CAB456', true, DATEADD(month, 2, CURRENT_DATE));
