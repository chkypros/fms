package com.gitlab.chkypros.fleetmanager.driver;

import com.gitlab.chkypros.fleetmanager.dao.Driver;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/drivers")
public class DriverController {

    private final DriverService service;

    @Autowired
    public DriverController(DriverService service) {
        this.service = service;
    }

    @ApiOperation("Get all driver records")
    @GetMapping
    public Iterable<Driver> getDrivers() {
        return service.getDrivers();
    }

    @ApiOperation("Get info of a specific driver")
    @GetMapping("/{id}")
    public Driver getDriver(@PathVariable final Long id) {
        return service.getById(id).orElse(null);
    }

    @ApiOperation("Register a new driver in the fleet")
    @PostMapping
    public Driver registerDriver(@RequestBody final Driver driver) {
        return service.registerDriver(driver);
    }

    @ApiOperation("Update the info of a driver")
    @PutMapping
    public Driver updateDriver(@RequestBody final Driver driver) {
        return service.updateDriver(driver);
    }

    @ApiOperation("Un-register a driver from the fleet")
    @DeleteMapping("/{id}")
    public void unregisterDriver(@PathVariable final Long id) {
        service.unregisterDriver(id);
    }
}
