package com.gitlab.chkypros.fleetmanager.car;

import com.gitlab.chkypros.fleetmanager.dao.Car;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cars")
public class CarController {
    private static final Logger LOG = LoggerFactory.getLogger(CarController.class);

    private final CarService service;

    @Autowired
    public CarController(CarService service) {
        this.service = service;
    }

    @ApiOperation("Get all car records")
    @GetMapping
    public Iterable<Car> getCars() {
        final var cars = service.getCars();
        LOG.info("Retrieving all cars ");
        return cars;
    }

    @ApiOperation("Get info of a specific car")
    @GetMapping("/{id}")
    public Car getCar(@PathVariable final Long id) {
        return service.getById(id).orElse(null);
    }

    @ApiOperation("Add a new car in the fleet")
    @PostMapping
    public Car addCar(@RequestBody final Car car) {
        return service.addCar(car);
    }

    @ApiOperation("Update the info of a car")
    @PutMapping
    public Car updateCar(@RequestBody final Car car) {
        return service.updateCar(car);
    }

    @ApiOperation("Remove a car from the fleet")
    @DeleteMapping("/{id}")
    public void removeCar(@PathVariable final Long id) {
        service.removeCar(id);
    }
}
