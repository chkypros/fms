package com.gitlab.chkypros.fleetmanager.driver;

import com.gitlab.chkypros.fleetmanager.dao.Driver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
public class DriverService {
    private final DriverRepository repository;

    @Autowired
    public DriverService(DriverRepository repository) {
        this.repository = repository;
    }

    public Iterable<Driver> getDrivers() {
        return repository.findAll();
    }

    public Optional<Driver> getById(final Long id) {
        return repository.findById(id);
    }

    public Driver registerDriver(final Driver driver) {
        StreamSupport.stream(repository.findAll().spliterator(), true)
                .filter(driver::equals)
                .findAny()
                .ifPresent(existingDriver -> {
                    throw new IllegalArgumentException("Driver is already registered with id=" + existingDriver.getId());
                });

        return repository.save(driver);
    }

    public Driver updateDriver(final Driver driver) {
        return repository.save(driver);
    }

    public void unregisterDriver(final Long id) {
        StreamSupport.stream(repository.findAll().spliterator(), true)
                .filter(existingDriver -> existingDriver.getId().equals(id))
                .findAny()
                .ifPresent(repository::delete);
    }
}
