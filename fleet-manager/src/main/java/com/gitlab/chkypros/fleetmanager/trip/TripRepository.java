package com.gitlab.chkypros.fleetmanager.trip;

import com.gitlab.chkypros.fleetmanager.dao.Trip;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TripRepository extends CrudRepository<Trip, Long> {

}
