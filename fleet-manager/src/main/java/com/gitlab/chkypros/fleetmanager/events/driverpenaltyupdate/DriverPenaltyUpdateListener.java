package com.gitlab.chkypros.fleetmanager.events.driverpenaltyupdate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.chkypros.fleetmanager.dao.Driver;
import com.gitlab.chkypros.fleetmanager.driver.DriverService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class DriverPenaltyUpdateListener {
    private static final String ID = "driver-penalty-update";
    private static final String TOPIC_NAME_PROPERTY = "${messaging.topics.driver-penalty-update}";

    private final ObjectMapper objectMapper;
    private final DriverService driverService;

    @Autowired
    public DriverPenaltyUpdateListener(ObjectMapper objectMapper, DriverService driverService) {
        this.objectMapper = objectMapper;
        this.driverService = driverService;
    }

    @KafkaListener(id = ID, topics = TOPIC_NAME_PROPERTY)
    public void listen(String driverPenaltyUpdateJson) {
        try {
            DriverPenaltyUpdate driverPenaltyUpdate = objectMapper.readValue(driverPenaltyUpdateJson, DriverPenaltyUpdate.class);
            log.info("Driver Penalty Update: {}", driverPenaltyUpdate);
            Optional<Driver> driverOptional = driverService.getById(driverPenaltyUpdate.getDriverId());
            if (driverOptional.isPresent()) {
                Driver driver = driverOptional.get();
                int currentPenaltyPoints = driver.getPenaltyPoints() == null ? 0 : driver.getPenaltyPoints();
                driver.setPenaltyPoints(currentPenaltyPoints + driverPenaltyUpdate.getPenaltyPoints());
                driverService.updateDriver(driver);
            } else {
                log.warn("Could not find driver with id={}", driverPenaltyUpdate.getDriverId());
            }
        } catch (JsonProcessingException e) {
            log.error("Failed to deserialize payload to type DriverPenaltyUpdate: {}" + driverPenaltyUpdateJson, e);
        }
    }
}
