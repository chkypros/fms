package com.gitlab.chkypros.fleetmanager.events.starttrip;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.chkypros.fleetmanager.dao.Trip;
import com.gitlab.chkypros.fleetmanager.trip.TripService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class StartTripMessagePublisher implements ApplicationListener<PayloadApplicationEvent<StartTripEvent>> {

    @Value("${messaging.topics.start-trip}")
    private String topic;

    private final KafkaTemplate<String, String> template;
    private final ObjectMapper objectMapper;
    private final TripService tripService;

    @Autowired
    public StartTripMessagePublisher(
            KafkaTemplate<String, String> template,
            ObjectMapper objectMapper,
            TripService tripService) {
        this.template = template;
        this.objectMapper = objectMapper;
        this.tripService = tripService;
    }

    @Override
    public void onApplicationEvent(PayloadApplicationEvent<StartTripEvent> payload) {
        StartTripEvent event = payload.getPayload();
        Optional<Trip> optionalTrip = tripService.getById(event.getTripId());
        if (optionalTrip.isPresent()) {
            Trip trip = optionalTrip.get();
            try {
                String tripJson = objectMapper.writeValueAsString(trip);
                log.info("Start Trip: {}", tripJson);
                template.send(topic, tripJson);
            } catch (JsonProcessingException e) {
                log.error("Failed to serialize the payload: " + trip, e);
            }
        } else {
            log.warn("Failed to retrieve trip with id:{}", event.getTripId());
        }
    }
}
