package com.gitlab.chkypros.fleetmanager.driver;

import com.gitlab.chkypros.fleetmanager.dao.Driver;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DriverRepository extends CrudRepository<Driver, Long> {

}
