package com.gitlab.chkypros.fleetmanager.car;

import com.gitlab.chkypros.fleetmanager.dao.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
public class CarService {

    private final CarRepository repository;

    @Autowired
    public CarService(CarRepository repository) {
        this.repository = repository;
    }

    public Iterable<Car> getCars() {
        return repository.findAll();
    }

    public Optional<Car> getById(final Long id) {
        return repository.findById(id);
    }

    public Car addCar(final Car car) {
        StreamSupport.stream(repository.findAll().spliterator(), true)
                .filter(car::equals)
                .findAny()
                .ifPresent(existingCar -> {
                    throw new IllegalArgumentException("Car is already registered with id=" + existingCar.getId());
                });

        return repository.save(car);
    }

    public Car updateCar(final Car Car) {
        return repository.save(Car);
    }

    public void removeCar(final Long id) {
        StreamSupport.stream(repository.findAll().spliterator(), true)
                .filter(existingCar -> existingCar.getId().equals(id))
                .findAny()
                .ifPresent(repository::delete);
    }
}
