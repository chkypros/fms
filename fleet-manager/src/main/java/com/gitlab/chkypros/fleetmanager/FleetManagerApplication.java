package com.gitlab.chkypros.fleetmanager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.config.TopicBuilder;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@SpringBootApplication
@EnableJpaRepositories
public class FleetManagerApplication {

    @Value("${messaging.topics.start-trip}")
    private String startTripTopic;

    @Value("${messaging.topics.change-trip-state}")
    private String changeTripStateTopic;

    @Value("${messaging.topics.driver-penalty-update}")
    private String driverPenaltyUpdate;

    public static void main(String[] args) {
        SpringApplication.run(FleetManagerApplication.class);
    }

    @Bean
    public NewTopic startTripTopic() {
        return TopicBuilder.name(startTripTopic)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic changeTripStateTopic() {
        return TopicBuilder.name(changeTripStateTopic)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic driverPenaltyUpdate() {
        return TopicBuilder.name(driverPenaltyUpdate)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Fleet Management System")
                .description("CRUD operations for Fleet and Trips management")
                .build();
    }
}
