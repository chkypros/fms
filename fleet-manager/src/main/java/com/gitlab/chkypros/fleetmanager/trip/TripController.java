package com.gitlab.chkypros.fleetmanager.trip;

import com.gitlab.chkypros.fleetmanager.dao.Trip;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trips")
public class TripController {

    private final TripService service;

    @Autowired
    public TripController(TripService service) {
        this.service = service;
    }

    @ApiOperation("Get all trip records")
    @GetMapping
    public Iterable<Trip> getTrips() {
        return service.getTrips();
    }

    @ApiOperation("Get record of a specific trip")
    @GetMapping("/{id}")
    public Trip getTrip(@PathVariable final Long id) {
        return service.getById(id).orElse(null);
    }

    @ApiOperation("Request a new trip")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Trip requestTrip(@RequestBody final TripRequest tripRequest) {
        return service.addTrip(tripRequest);
    }

    @ApiOperation("Update the info of a trip record")
    @PutMapping
    public Trip updateTrip(@RequestBody final Trip trip) {
        return service.updateTrip(trip);
    }

    @ApiOperation("Remove trip from records")
    @DeleteMapping("/{id}")
    public void removeTrip(@PathVariable final Long id) {
        service.removeTrip(id);
    }
}
