package com.gitlab.chkypros.fleetmanager.dao;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class Driver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(hidden = true)
    private Long id;

    @EqualsAndHashCode.Include
    private String nationalId;

    private String firstName;

    private String lastName;

    private Double speedingTendency;

    private Integer penaltyPoints;

    public Driver(
            String nationalId,
            String firstName,
            String lastName,
            Double speedingTendency,
            Integer penaltyPoints) {
        this.nationalId = nationalId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.speedingTendency = speedingTendency;
        this.penaltyPoints = penaltyPoints;
    }
}
