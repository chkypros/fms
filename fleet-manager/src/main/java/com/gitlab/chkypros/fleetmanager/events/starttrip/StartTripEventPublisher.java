package com.gitlab.chkypros.fleetmanager.events.starttrip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class StartTripEventPublisher {
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public StartTripEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void publish(StartTripEvent event) {
        applicationEventPublisher.publishEvent(event);
    }
}
