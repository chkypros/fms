package com.gitlab.chkypros.fleetmanager.trip;

import com.gitlab.chkypros.fleetmanager.car.CarService;
import com.gitlab.chkypros.fleetmanager.dao.Car;
import com.gitlab.chkypros.fleetmanager.dao.Driver;
import com.gitlab.chkypros.fleetmanager.dao.Trip;
import com.gitlab.chkypros.fleetmanager.domain.TripState;
import com.gitlab.chkypros.fleetmanager.driver.DriverService;
import com.gitlab.chkypros.fleetmanager.events.starttrip.StartTripEvent;
import com.gitlab.chkypros.fleetmanager.events.starttrip.StartTripEventPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
public class TripService {

    private final TripRepository repository;
    private final DriverService driverService;
    private final CarService carService;
    private final StartTripEventPublisher startTripEventPublisher;

    @Autowired
    public TripService(
            TripRepository repository,
            DriverService driverService,
            CarService carService,
            StartTripEventPublisher startTripEventPublisher) {
        this.repository = repository;
        this.driverService = driverService;
        this.carService = carService;
        this.startTripEventPublisher = startTripEventPublisher;
    }

    public Iterable<Trip> getTrips() {
        return repository.findAll();
    }

    public Optional<Trip> getById(final Long id) {
        return repository.findById(id);
    }

    public Trip addTrip(final TripRequest tripRequest) {
        Driver driver = driverService.getById(tripRequest.getDriverId())
                .orElseThrow(() -> new IllegalArgumentException("No driver found with id=" + tripRequest.getDriverId()));
        Car car = carService.getById(tripRequest.getCarId())
                .orElseThrow(() -> new IllegalArgumentException("No car found with id=" + tripRequest.getCarId()));

        final Trip trip = Trip.builder()
                .withStart(tripRequest.getStart())
                .withDestination(tripRequest.getDestination())
                .withCar(car)
                .withDriver(driver)
                .withState(TripState.IN_BASE)
                .withRequestTime(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES))
                .build();

        StreamSupport.stream(repository.findAll().spliterator(), true)
                .filter(trip::equals)
                .findAny()
                .ifPresent(existingTrip -> {
                    throw new IllegalArgumentException("Trip is already registered with id=" + existingTrip.getId());
                });

        Trip savedTrip = repository.save(trip);
        startTripEventPublisher.publish(new StartTripEvent(savedTrip.getId()));

        return savedTrip;
    }

    public Trip updateTrip(final Trip Trip) {
        return repository.save(Trip);
    }

    public void removeTrip(final Long id) {
        StreamSupport.stream(repository.findAll().spliterator(), true)
                .filter(existingTrip -> existingTrip.getId().equals(id))
                .findAny()
                .ifPresent(repository::delete);
    }
}
