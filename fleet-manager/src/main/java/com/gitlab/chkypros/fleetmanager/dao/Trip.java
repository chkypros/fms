package com.gitlab.chkypros.fleetmanager.dao;

import com.gitlab.chkypros.fleetmanager.domain.Coordinates;
import com.gitlab.chkypros.fleetmanager.domain.TripState;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@Builder(setterPrefix = "with")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ToString.Exclude
    @ApiModelProperty(hidden = true)
    private Long id;

    private Coordinates start;

    private Coordinates destination;

    @ManyToOne
    @EqualsAndHashCode.Include
    private Driver driver;

    @ManyToOne
    private Car car;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(hidden = true)
    private TripState state;

    @EqualsAndHashCode.Include
    @ApiModelProperty(hidden = true)
    private LocalDateTime requestTime;
}
