package com.gitlab.chkypros.fleetmanager.events.changetripstate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.chkypros.fleetmanager.dao.Trip;
import com.gitlab.chkypros.fleetmanager.trip.TripService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
public class ChangeTripStateListener {
    private static final String ID = "change-trip-state";
    private static final String TOPIC_NAME_PROPERTY = "${messaging.topics.change-trip-state}";

    private final ObjectMapper objectMapper;
    private final TripService tripService;

    @Autowired
    public ChangeTripStateListener(ObjectMapper objectMapper, TripService tripService) {
        this.objectMapper = objectMapper;
        this.tripService = tripService;
    }

    @KafkaListener(id = ID, topics = TOPIC_NAME_PROPERTY)
    public void listen(String changeTripStateJson) {
        try {
            ChangeTripStateEvent event = objectMapper.readValue(changeTripStateJson, ChangeTripStateEvent.class);
            log.info("Change trip state: {}", event);
            Optional<Trip> tripOptional = tripService.getById(event.getTripId());
            if (tripOptional.isPresent()) {
                Trip trip = tripOptional.get();
                trip.setState(event.getNewState());
                tripService.updateTrip(trip);
            } else {
                log.warn("Could not find trip with id={}", event.getTripId());
            }
        } catch (JsonProcessingException e) {
            log.error("Failed to deserialize payload to type ChangeTripStateEvent: {}" + changeTripStateJson, e);
        }
    }
}
