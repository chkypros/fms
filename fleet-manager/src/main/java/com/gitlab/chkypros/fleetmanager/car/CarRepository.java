package com.gitlab.chkypros.fleetmanager.car;

import com.gitlab.chkypros.fleetmanager.dao.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {

}
