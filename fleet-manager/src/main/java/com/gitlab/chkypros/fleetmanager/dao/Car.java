package com.gitlab.chkypros.fleetmanager.dao;

import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(hidden = true)
    private Long id;

    private String brand;

    private String model;

    @EqualsAndHashCode.Include
    private String licencePlate;

    private Boolean automatic;

    private LocalDate nextServiceDate;

    public Car(String brand, String model, String licencePlate, Boolean automatic, LocalDate nextServiceDate) {
        this.brand = brand;
        this.model = model;
        this.licencePlate = licencePlate;
        this.automatic = automatic;
        this.nextServiceDate = nextServiceDate;
    }
}
