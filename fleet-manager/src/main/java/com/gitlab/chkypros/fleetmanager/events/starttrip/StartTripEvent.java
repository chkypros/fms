package com.gitlab.chkypros.fleetmanager.events.starttrip;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class StartTripEvent {
    private Long tripId;

    @Override
    public String toString() {
        return tripId.toString();
    }
}
