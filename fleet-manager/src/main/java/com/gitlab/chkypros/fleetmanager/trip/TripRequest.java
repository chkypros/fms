package com.gitlab.chkypros.fleetmanager.trip;

import com.gitlab.chkypros.fleetmanager.domain.Coordinates;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TripRequest {
    @ApiModelProperty(example = "1")
    private Long driverId;

    @ApiModelProperty(example = "1")
    private Long carId;

    private Coordinates start;

    private Coordinates destination;
}
