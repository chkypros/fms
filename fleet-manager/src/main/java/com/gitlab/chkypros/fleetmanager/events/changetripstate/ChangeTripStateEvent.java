package com.gitlab.chkypros.fleetmanager.events.changetripstate;

import com.gitlab.chkypros.fleetmanager.domain.TripState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ChangeTripStateEvent {
    private Long tripId;
    private TripState newState;
}
