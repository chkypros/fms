package com.gitlab.chkypros.tripagent.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OnGoingTrip {
    private final Trip trip;
    private Coordinates position;
    private Coordinates nextTarget;
    private Double speed;

    public OnGoingTrip(final Trip trip, final Coordinates currentPosition) {
        this.trip = trip;
        this.position = currentPosition;
        this.nextTarget = trip.getStart();
        this.speed = 0.0;

        this.trip.setState(TripState.TOWARDS_CUSTOMER);
    }

    public Driver getDriver() {
        return getTrip().getDriver();
    }

    public Car getCar() {
        return getTrip().getCar();
    }

    public TripState getState() {
        return getTrip().getState();
    }

    public void setState(TripState tripState) {
        getTrip().setState(tripState);
    }
}
