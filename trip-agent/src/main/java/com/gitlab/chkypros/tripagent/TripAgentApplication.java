package com.gitlab.chkypros.tripagent;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class TripAgentApplication {

    @Value("${messaging.topics.start-trip}")
    private String startTripTopic;

    @Value("${messaging.topics.change-trip-state}")
    private String changeTripStateTopic;

    @Value("${messaging.topics.heartbeat}")
    private String heartBeatTopic;

    public static void main(String[] args) {
        SpringApplication.run(TripAgentApplication.class);
    }

    @Bean
    public NewTopic startTripTopic() {
        return TopicBuilder.name(startTripTopic)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic heartBeatTopic() {
        return TopicBuilder.name(heartBeatTopic)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic changeTripStateTopic() {
        return TopicBuilder.name(changeTripStateTopic)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }
}
