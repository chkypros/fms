package com.gitlab.chkypros.tripagent.events.starttrip;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.chkypros.tripagent.domain.Trip;
import com.gitlab.chkypros.tripagent.manager.TripManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StartTripListener {
    private static final String ID = "start-trip";
    private static final String TOPIC_NAME_PROPERTY = "${messaging.topics.start-trip}";

    private final ObjectMapper objectMapper;
    private final TripManager tripManager;

    @Autowired
    public StartTripListener(ObjectMapper objectMapper, TripManager tripManager) {
        this.objectMapper = objectMapper;
        this.tripManager = tripManager;
    }

    @KafkaListener(id = ID, topics = TOPIC_NAME_PROPERTY)
    public void listen(String tripJson) {
        try {
            Trip trip = objectMapper.readValue(tripJson, Trip.class);
            log.info("Start trip: {}", trip);
            tripManager.initiateTrip(trip);
        } catch (JsonProcessingException e) {
            log.error("Failed to deserialize payload to type Trip: {}", tripJson);
        }
    }
}
