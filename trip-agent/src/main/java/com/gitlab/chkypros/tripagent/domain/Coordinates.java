package com.gitlab.chkypros.tripagent.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.io.Serializable;

@Value
public class Coordinates implements Serializable {

    int x;
    int y;

    @JsonCreator
    public Coordinates(@JsonProperty("x") int x, @JsonProperty("y") int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("[%d, %d]", x, y);
    }

    public static double distance(final Coordinates positionA, final Coordinates positionB) {
        int yDiff = Math.abs(positionA.getY()) - positionB.getY();
        int xDiff = Math.abs(positionA.getX()) - positionB.getX();
        return Math.hypot(yDiff, xDiff);
    }
}
