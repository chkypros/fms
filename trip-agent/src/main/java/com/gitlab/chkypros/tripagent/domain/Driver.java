package com.gitlab.chkypros.tripagent.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Driver {

    private Long id;
    private String nationalId;
    private String firstName;
    private String lastName;
    private Double speedingTendency;
    private Integer penaltyPoints;

    public Driver(
            String nationalId,
            String firstName,
            String lastName,
            Double speedingTendency,
            Integer penaltyPoints) {
        this.nationalId = nationalId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.speedingTendency = speedingTendency;
        this.penaltyPoints = penaltyPoints;
    }

}
