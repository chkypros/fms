package com.gitlab.chkypros.tripagent.events.changetripstate;

import com.gitlab.chkypros.tripagent.domain.TripState;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ChangeTripStateEvent {
    private final Long tripId;
    private final TripState newState;
}
