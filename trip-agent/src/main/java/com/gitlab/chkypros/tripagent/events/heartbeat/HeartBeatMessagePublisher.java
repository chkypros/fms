package com.gitlab.chkypros.tripagent.events.heartbeat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.chkypros.tripagent.domain.HeartBeat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HeartBeatMessagePublisher {

    @Value("${messaging.topics.heartbeat}")
    private String topic;

    private final KafkaTemplate<String, String> template;
    private final ObjectMapper objectMapper;

    @Autowired
    public HeartBeatMessagePublisher(KafkaTemplate<String, String> template, ObjectMapper objectMapper) {
        this.template = template;
        this.objectMapper = objectMapper;
    }

    public void publish(HeartBeat heartBeat) {
        try {
            String heartBeatJson = objectMapper.writeValueAsString(heartBeat);
            log.debug("Heartbeat: {}", heartBeatJson);
            template.send(topic, heartBeatJson);
        } catch (JsonProcessingException e) {
            log.error("Failed to serialize the payload: " + heartBeat, e);
        }
    }
}
