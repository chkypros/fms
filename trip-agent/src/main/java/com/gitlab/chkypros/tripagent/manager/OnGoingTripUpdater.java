package com.gitlab.chkypros.tripagent.manager;

import com.gitlab.chkypros.tripagent.domain.Coordinates;
import com.gitlab.chkypros.tripagent.domain.OnGoingTrip;
import com.gitlab.chkypros.tripagent.domain.TripState;
import com.gitlab.chkypros.tripagent.events.changetripstate.ChangeTripStateEvent;
import com.gitlab.chkypros.tripagent.events.changetripstate.ChangeTripStateEventPublisher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class OnGoingTripUpdater {
    @Value("${trip.ongoing.min.speed:50}")
    private int minSpeed;

    @Value("${trip.ongoing.reached.destination.distance.threshold:3}")
    private int reachedDestinationDistanceThreshold;

    private final Random random = new Random();
    private final Coordinates basePosition;

    private final ChangeTripStateEventPublisher changeTripStateEventPublisher;

    public OnGoingTripUpdater(
            ChangeTripStateEventPublisher changeTripStateEventPublisher,
            @Value("${base.position.x}") int basePositionX,
            @Value("${base.position.y}") int basePositionY
    ) {
        this.changeTripStateEventPublisher = changeTripStateEventPublisher;
        this.basePosition = new Coordinates(basePositionX, basePositionY);
    }

    public void update(OnGoingTrip onGoingTrip) {
        onGoingTrip.setPosition(calculateNextPosition(onGoingTrip));

        if (onGoingTrip.getPosition().equals(onGoingTrip.getNextTarget())) {
            updateStateAndNextTarget(onGoingTrip);
            ChangeTripStateEvent event =
                    new ChangeTripStateEvent(onGoingTrip.getTrip().getId(), onGoingTrip.getState());
            changeTripStateEventPublisher.publish(event);
        }

        if (TripState.PICKING_UP_CUSTOMER.equals(onGoingTrip.getState())
            || TripState.FINISHED.equals(onGoingTrip.getState())) {
            onGoingTrip.setSpeed(0.0);
        } else {
            onGoingTrip.setSpeed(minSpeed + random.nextDouble() * onGoingTrip.getDriver().getSpeedingTendency());
        }
    }

    private Coordinates calculateNextPosition(OnGoingTrip onGoingTrip) {
        final Coordinates position = onGoingTrip.getPosition();
        final Coordinates nextTarget = onGoingTrip.getNextTarget();
        if (Coordinates.distance(position, nextTarget) < reachedDestinationDistanceThreshold) {
            return nextTarget;
        }

        int nextX = position.getX();
        if (position.getX() < nextTarget.getX()) {
            nextX = position.getX() + random.nextInt(nextTarget.getX() - position.getX());
        } else if (position.getX() > nextTarget.getX()) {
            nextX = position.getX() - random.nextInt(position.getX() - nextTarget.getX());
        }

        int nextY = position.getY();
        if (position.getY() < nextTarget.getY()) {
            nextY = position.getY() + random.nextInt(nextTarget.getY() - position.getY());
        } else if (position.getY() > nextTarget.getY()) {
            nextY = position.getY() - random.nextInt(position.getY() - nextTarget.getY());
        }

        return new Coordinates(nextX, nextY);
    }

    private void updateStateAndNextTarget(OnGoingTrip onGoingTrip) {
        TripState newState = onGoingTrip.getState();
        Coordinates newNextTarget = onGoingTrip.getNextTarget();

        switch (onGoingTrip.getState()) {
            case IN_BASE:
                newState = TripState.TOWARDS_CUSTOMER;
                newNextTarget = onGoingTrip.getTrip().getStart();
                break;
            case TOWARDS_CUSTOMER:
                newState = TripState.PICKING_UP_CUSTOMER;
                break;
            case PICKING_UP_CUSTOMER:
                newState = TripState.TOWARDS_DESTINATION;
                newNextTarget = onGoingTrip.getTrip().getDestination();
                break;
            case TOWARDS_DESTINATION:
                newState = TripState.TOWARDS_BASE;
                newNextTarget = basePosition;
                break;
            case TOWARDS_BASE:
                newState = TripState.FINISHED;
                break;
        }

        onGoingTrip.setState(newState);
        onGoingTrip.setNextTarget(newNextTarget);
    }
}
