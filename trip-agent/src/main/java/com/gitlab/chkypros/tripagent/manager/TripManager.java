package com.gitlab.chkypros.tripagent.manager;

import com.gitlab.chkypros.tripagent.domain.*;
import com.gitlab.chkypros.tripagent.events.heartbeat.HeartBeatMessagePublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
public class TripManager {

    private final Set<OnGoingTrip> ongoingTrips = new HashSet<>();
    private final Coordinates basePosition;

    private final OnGoingTripUpdater onGoingTripUpdater;
    private final HeartBeatMessagePublisher heartBeatMessagePublisher;

    @Autowired
    public TripManager(
            OnGoingTripUpdater onGoingTripUpdater,
            HeartBeatMessagePublisher heartBeatMessagePublisher,
            @Value("${base.position.x}") int basePositionX,
            @Value("${base.position.y}") int basePositionY
    ) {
        this.onGoingTripUpdater = onGoingTripUpdater;
        this.heartBeatMessagePublisher = heartBeatMessagePublisher;
        this.basePosition = new Coordinates(basePositionX, basePositionY);
    }

    public void initiateTrip(final Trip trip) {
        ongoingTrips.add(new OnGoingTrip(trip, basePosition));
    }

    @Scheduled(fixedRateString = "${heartbeat.interval:1000}")
    private void heartbeat() {
        ongoingTrips.forEach(this::beat);
    }

    private void beat(OnGoingTrip onGoingTrip) {
        onGoingTripUpdater.update(onGoingTrip);
        log.info("Beat for trip:{}", onGoingTrip);

        HeartBeat heartBeat = new HeartBeat();
        heartBeat.setTripId(onGoingTrip.getTrip().getId());
        heartBeat.setDriverId(onGoingTrip.getDriver().getId());
        heartBeat.setCarId(onGoingTrip.getCar().getId());
        heartBeat.setPosition(onGoingTrip.getPosition());
        heartBeat.setSpeed(onGoingTrip.getSpeed());
        heartBeatMessagePublisher.publish(heartBeat);

        if (TripState.FINISHED.equals(onGoingTrip.getState())) {
            ongoingTrips.remove(onGoingTrip);
        }
    }
}
