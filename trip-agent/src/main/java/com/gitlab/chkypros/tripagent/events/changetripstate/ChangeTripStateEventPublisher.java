package com.gitlab.chkypros.tripagent.events.changetripstate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class ChangeTripStateEventPublisher {
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public ChangeTripStateEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void publish(ChangeTripStateEvent event) {
        applicationEventPublisher.publishEvent(event);
    }
}
