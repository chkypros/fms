package com.gitlab.chkypros.tripagent.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Car {

    private Long id;
    private String brand;
    private String model;
    private String licencePlate;
    private Boolean automatic;
    private LocalDate nextServiceDate;

    public Car(String brand, String model, String licencePlate, Boolean automatic, LocalDate nextServiceDate) {
        this.brand = brand;
        this.model = model;
        this.licencePlate = licencePlate;
        this.automatic = automatic;
        this.nextServiceDate = nextServiceDate;
    }

}
