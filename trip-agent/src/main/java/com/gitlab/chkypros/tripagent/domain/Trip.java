package com.gitlab.chkypros.tripagent.domain;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Trip {

    private Long id;
    private Coordinates start;
    private Coordinates destination;
    private Driver driver;
    private Car car;
    private TripState state;
    private LocalDateTime requestTime;

}
