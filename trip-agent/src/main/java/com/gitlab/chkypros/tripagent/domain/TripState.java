package com.gitlab.chkypros.tripagent.domain;

public enum TripState {
    IN_BASE,
    TOWARDS_CUSTOMER,
    PICKING_UP_CUSTOMER,
    TOWARDS_DESTINATION,
    TOWARDS_BASE,
    FINISHED,
}
