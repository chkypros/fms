package com.gitlab.chkypros.tripagent.events.changetripstate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ChangeTripStateMessagePublisher implements ApplicationListener<PayloadApplicationEvent<ChangeTripStateEvent>> {
    @Value("${messaging.topics.change-trip-state}")
    private String topic;

    private final KafkaTemplate<String, String> template;
    private final ObjectMapper objectMapper;

    @Autowired
    public ChangeTripStateMessagePublisher(KafkaTemplate<String, String> template, ObjectMapper objectMapper) {
        this.template = template;
        this.objectMapper = objectMapper;
    }

    @Override
    public void onApplicationEvent(PayloadApplicationEvent<ChangeTripStateEvent> payload) {
        ChangeTripStateEvent event = payload.getPayload();
        try {
            String eventJson = objectMapper.writeValueAsString(event);
            log.info("Start Trip: {}", eventJson);
            template.send(topic, eventJson);
        } catch (JsonProcessingException e) {
            log.error("Failed to serialize the payload: " + event, e);
        }
    }
}
