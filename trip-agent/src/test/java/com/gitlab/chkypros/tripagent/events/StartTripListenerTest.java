package com.gitlab.chkypros.tripagent.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.chkypros.tripagent.domain.Trip;
import com.gitlab.chkypros.tripagent.events.starttrip.StartTripListener;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@SpringBootTest
@DirtiesContext
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = {
                "listeners=PLAINTEXT://localhost:29092",
                "port=29092"
        }
)
class StartTripListenerTest {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final long TIMEOUT = 5000L;

    @SpyBean
    private StartTripListener startTripListener;

    @Autowired
    private KafkaTemplate<String, String> template;

    @Value("${messaging.topics.start-trip}")
    private String topic;

    @Test
    void listen() throws Exception {
        String messagePayload = OBJECT_MAPPER.writeValueAsString(new Trip());

        template.send(topic, messagePayload).get();

        verify(startTripListener, timeout(TIMEOUT))
                .listen(anyString());
    }
}
