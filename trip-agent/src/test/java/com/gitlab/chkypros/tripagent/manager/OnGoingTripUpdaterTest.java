package com.gitlab.chkypros.tripagent.manager;

import com.gitlab.chkypros.tripagent.domain.*;
import com.gitlab.chkypros.tripagent.events.changetripstate.ChangeTripStateEventPublisher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.ReflectionUtils;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class OnGoingTripUpdaterTest {

    private static final int BASE_POSITION_X = 20;
    private static final int BASE_POSITION_Y = 20;

    private static final Coordinates CURRENT_POSITION = new Coordinates(20, 30);
    private static final Coordinates START_POSITION = new Coordinates(10, 20);
    private static final Coordinates DESTINATION_POSITION = new Coordinates(40, 50);

    @Mock
    private ChangeTripStateEventPublisher changeTripStateEventPublisher;

    private OnGoingTripUpdater updater;

    @BeforeEach
    void setUp() throws Exception {
        updater = new OnGoingTripUpdater(changeTripStateEventPublisher, BASE_POSITION_X, BASE_POSITION_Y);

        setupPrivateFields();
    }

    private void setupPrivateFields() throws NoSuchFieldException, IllegalAccessException {
        Field minSpeedField = updater.getClass().getDeclaredField("minSpeed");
        minSpeedField.setAccessible(true);
        minSpeedField.setInt(updater, 50);

        Field reachedDestinationDistanceThresholdField = updater.getClass().getDeclaredField("reachedDestinationDistanceThreshold");
        reachedDestinationDistanceThresholdField.setAccessible(true);
        reachedDestinationDistanceThresholdField.setInt(updater, 3);
    }

    @Test
    void update_check_progress() {
        // Given
        Trip trip = getTrip();
        trip.setState(TripState.TOWARDS_DESTINATION);
        OnGoingTrip onGoingTrip = new OnGoingTrip(trip, CURRENT_POSITION);
        onGoingTrip.setNextTarget(trip.getDestination());

        // When
        updater.update(onGoingTrip);

        // Then
        Coordinates newPosition = onGoingTrip.getPosition();
        assertTrue(newPosition.getX() >= CURRENT_POSITION.getX());
        assertTrue(newPosition.getY() >= CURRENT_POSITION.getY());
    }

    @Test
    void update_check_state_change() {
        // Given
        Trip trip = getTrip();
        trip.setState(TripState.TOWARDS_CUSTOMER);
        OnGoingTrip onGoingTrip = new OnGoingTrip(trip, trip.getStart());
        onGoingTrip.setNextTarget(trip.getStart());

        // When
        updater.update(onGoingTrip);

        // Then
        assertEquals(TripState.PICKING_UP_CUSTOMER, onGoingTrip.getState());
    }

    private Trip getTrip() {
        Trip trip = new Trip();
        trip.setState(TripState.TOWARDS_CUSTOMER);
        trip.setStart(START_POSITION);
        trip.setDestination(DESTINATION_POSITION);
        Driver driver = new Driver();
        driver.setSpeedingTendency(2.0);
        trip.setDriver(driver);
        return trip;
    }
}