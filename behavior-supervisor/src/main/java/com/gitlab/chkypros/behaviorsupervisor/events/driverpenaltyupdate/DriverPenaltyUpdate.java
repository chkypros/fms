package com.gitlab.chkypros.behaviorsupervisor.events.driverpenaltyupdate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DriverPenaltyUpdate {
    private Long driverId;
    private Integer penaltyPoints;
}
