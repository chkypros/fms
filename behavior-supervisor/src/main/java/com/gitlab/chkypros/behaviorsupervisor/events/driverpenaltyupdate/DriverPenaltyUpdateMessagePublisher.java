package com.gitlab.chkypros.behaviorsupervisor.events.driverpenaltyupdate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DriverPenaltyUpdateMessagePublisher {

    @Value("${messaging.topics.driver-penalty-update}")
    private String topic;

    private final KafkaTemplate<String, String> template;
    private final ObjectMapper objectMapper;

    @Autowired
    public DriverPenaltyUpdateMessagePublisher(
            KafkaTemplate<String, String> template,
            ObjectMapper objectMapper) {
        this.template = template;
        this.objectMapper = objectMapper;
    }

    public void publish(DriverPenaltyUpdate driverPenaltyUpdate) {
        try {
            String driverPenaltyUpdateJson = objectMapper.writeValueAsString(driverPenaltyUpdate);
            log.info("Sending DriverPenaltyUpdate: {}", driverPenaltyUpdateJson);
            template.send(topic, driverPenaltyUpdateJson);
        } catch (JsonProcessingException e) {
            log.error("Failed to serialize the payload: " + driverPenaltyUpdate, e);
        }
    }
}
