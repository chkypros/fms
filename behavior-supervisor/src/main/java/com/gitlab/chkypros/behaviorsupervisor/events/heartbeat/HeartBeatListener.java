package com.gitlab.chkypros.behaviorsupervisor.events.heartbeat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.chkypros.behaviorsupervisor.domain.HeartBeat;
import com.gitlab.chkypros.behaviorsupervisor.service.BehaviorEvaluator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HeartBeatListener {

    private static final String ID = "heartbeat";
    private static final String TOPIC_NAME_PROPERTY = "${messaging.topics.heartbeat}";

    private final ObjectMapper objectMapper;
    private final BehaviorEvaluator behaviorEvaluator;

    @Autowired
    public HeartBeatListener(ObjectMapper objectMapper, BehaviorEvaluator behaviorEvaluator) {
        this.objectMapper = objectMapper;
        this.behaviorEvaluator = behaviorEvaluator;
    }

    @KafkaListener(id = ID, topics = TOPIC_NAME_PROPERTY)
    public void listen(String heartbeatJson) {
        try {
            HeartBeat heartBeat = objectMapper.readValue(heartbeatJson, HeartBeat.class);
            log.info("HeartBeat: {}", heartBeat);
            behaviorEvaluator.evaluate(heartBeat);
        } catch (JsonProcessingException e) {
            log.error("Failed to deserialize payload to type HeartBeat: {}", heartbeatJson);
        }
    }
}
