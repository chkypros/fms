package com.gitlab.chkypros.behaviorsupervisor.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class HeartBeat {
    private Long tripId;
    private Long carId;
    private Long driverId;
    private Coordinates position;
    private Double speed;
}
