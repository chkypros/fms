package com.gitlab.chkypros.behaviorsupervisor.service;

import com.gitlab.chkypros.behaviorsupervisor.config.BehaviorPenaltiesConfiguration;
import com.gitlab.chkypros.behaviorsupervisor.domain.HeartBeat;
import com.gitlab.chkypros.behaviorsupervisor.events.driverpenaltyupdate.DriverPenaltyUpdate;
import com.gitlab.chkypros.behaviorsupervisor.events.driverpenaltyupdate.DriverPenaltyUpdateMessagePublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Component
public class BehaviorEvaluator {
    private final BehaviorPenaltiesConfiguration behaviorPenaltiesConfiguration;
    private final DriverPenaltyUpdateMessagePublisher driverPenaltyUpdateMessagePublisher;

    @Autowired
    public BehaviorEvaluator(
            BehaviorPenaltiesConfiguration behaviorPenaltiesConfiguration,
            DriverPenaltyUpdateMessagePublisher driverPenaltyUpdateMessagePublisher) {
        this.behaviorPenaltiesConfiguration = behaviorPenaltiesConfiguration;
        this.driverPenaltyUpdateMessagePublisher = driverPenaltyUpdateMessagePublisher;
    }

    public void evaluate(HeartBeat heartBeat) {
        Map<Integer, Integer> speedPenalties = behaviorPenaltiesConfiguration.getSpeed();
        List<Integer> speedLimitsPassed = speedPenalties.keySet().stream().sorted()
                .filter(speed -> heartBeat.getSpeed() >= speed)
                .collect(Collectors.toList());

        if (!speedLimitsPassed.isEmpty()) {
            int penaltyPoints = 0;
            int lastIndex = speedLimitsPassed.size() - 1;
            for (int i = 0; i < lastIndex; i++) {
                penaltyPoints += (speedLimitsPassed.get(i + 1) - speedLimitsPassed.get(i)) * speedPenalties.get(speedLimitsPassed.get(i));
            }
            penaltyPoints += (heartBeat.getSpeed() - speedLimitsPassed.get(lastIndex)) * speedPenalties.get(speedLimitsPassed.get(lastIndex));

            DriverPenaltyUpdate driverPenaltyUpdate = new DriverPenaltyUpdate();
            driverPenaltyUpdate.setDriverId(heartBeat.getDriverId());
            driverPenaltyUpdate.setPenaltyPoints(penaltyPoints);
            driverPenaltyUpdateMessagePublisher.publish(driverPenaltyUpdate);
        }
    }
}
