package com.gitlab.chkypros.behaviorsupervisor;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;

@SpringBootApplication
public class BehaviorSupervisorApplication {

    @Value("${messaging.topics.heartbeat}")
    private String heartBeatTopic;

    @Value("${messaging.topics.driver-penalty-update}")
    private String driverPenaltyUpdate;

    public static void main(String[] args) {
        SpringApplication.run(BehaviorSupervisorApplication.class);
    }

    @Bean
    public NewTopic heartBeatTopic() {
        return TopicBuilder.name(heartBeatTopic)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public NewTopic driverPenaltyUpdate() {
        return TopicBuilder.name(driverPenaltyUpdate)
                .partitions(10)
                .replicas(1)
                .build();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
