package com.gitlab.chkypros.behaviorsupervisor.service;

import com.gitlab.chkypros.behaviorsupervisor.config.BehaviorPenaltiesConfiguration;
import com.gitlab.chkypros.behaviorsupervisor.domain.Coordinates;
import com.gitlab.chkypros.behaviorsupervisor.domain.HeartBeat;
import com.gitlab.chkypros.behaviorsupervisor.events.driverpenaltyupdate.DriverPenaltyUpdate;
import com.gitlab.chkypros.behaviorsupervisor.events.driverpenaltyupdate.DriverPenaltyUpdateMessagePublisher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BehaviorEvaluatorTest {

    private static final Map<Integer, Integer> SPEED_PENALTY_MAP = Map.of(
            60, 6,
            80, 10
    );

    @Mock
    private BehaviorPenaltiesConfiguration behaviorPenaltiesConfiguration;

    @Mock
    private DriverPenaltyUpdateMessagePublisher driverPenaltyUpdateMessagePublisher;

    @InjectMocks
    private BehaviorEvaluator evaluator;

    @Captor
    private ArgumentCaptor<DriverPenaltyUpdate> updateCaptor;

    @BeforeEach
    void setUp() {
        when(behaviorPenaltiesConfiguration.getSpeed()).thenReturn(SPEED_PENALTY_MAP);
    }

    @ParameterizedTest
    @CsvSource({
            "70.0, 60",
            "90.0, 220"
    })
    void evaluate_over_limit(double speed, int expectedPenaltyPoints) {
        // When
        evaluator.evaluate(getHeartBeatForSpeed(speed));

        // Then
        verify(driverPenaltyUpdateMessagePublisher).publish(updateCaptor.capture());
        assertEquals(expectedPenaltyPoints, updateCaptor.getValue().getPenaltyPoints());
    }

    @Test
    void evaluate_under_limit() {
        // When
        evaluator.evaluate(getHeartBeatForSpeed(50.0));

        // Then
        verifyNoInteractions(driverPenaltyUpdateMessagePublisher);
    }

    private HeartBeat getHeartBeatForSpeed(Double speed) {
        HeartBeat heartBeat = new HeartBeat();
        heartBeat.setSpeed(speed);
        heartBeat.setCarId(1L);
        heartBeat.setDriverId(1L);
        heartBeat.setTripId(1L);
        heartBeat.setPosition(new Coordinates(1, 1));

        return heartBeat;
    }
}