package com.gitlab.chkypros.behaviorsupervisor;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext
@EmbeddedKafka(
        partitions = 1,
        brokerProperties = {
                "listeners=PLAINTEXT://localhost:39092",
                "port=39092"
        }
)
class BehaviorSupervisorApplicationTests {

    @Test
    void contextLoads() {
    }

}
