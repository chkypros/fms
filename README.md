# FMS

A fictitious microservice-based Fleet Management System using Spring Boot and Java.
The system provides functionality for managing fleet resources, Drivers, Cars and Trips.
Furthermore, it enables tracking of the progress of each trip
and keeping score of penalty points for each driver,
based on their travelling speed.

## Solution Overview

![Solution Overview](TRG_Assessment.png)

The system consists of three microservices: FleetManager, TripAgent and BehaviorSupervisor.

### FleetManager
The FleetManager service provides CRUD operations for managing the fleet.
It further initiates trips on request.
When a new trip is requested, a "start-trip" message is published.
During the lifetime of that trip, "update-trip-state" messages are consumed and the state is updated in the DB.
At the same time, "driver-penalty-update" messages are consumed when a driver is driving over the limit.


### TripAgent
The TripAgent service keeps track of all the on-going trips,
publishing "heartbeat" messages on fixed intervals, with the trip, car and driver system ids.
The heartbeat messages also include the current position and speed of the car.
Whenever the trip reaches an intermediate destination, the new trip state is calculated and published with a "update-trip-state" message.

The trip state flow is as follows:
1. In Base
2. Moving towards customer
3. Picking up customer
4. Moving towards destination
5. Returning to base
6. Finished

### BehaviorSupervisor
The BehaviorSupervisor service evaluates the behavior of the drivers on trips.
It currently checks their travelling speed and, given they are over the limit,
calculates the respective penalty points which are then published through "driver-penalty-update" messages.

## Assumptions
1. The system assumes the trips start and finish at the base (office).
2. Cars can be driven by any driver
   1. For each trip a different Car-Driver pair can be assigned
